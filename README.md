## Sexual Rights Database CMS

The CMS is based on Drupal 7.

## DDEV environment configuration


In the DDEV environment, since each repo will be running in its own set of containers, networking can be tricky. Configuring the hostnames as something like srrl-cms.ddev.site will always fail because \*.ddev.site resolves to 127.0.0.1, so it will resolve to the same container making the request, which doesn't run the required service.

The solution is to use the container name of the target service. For example, with the current default configuration, https://ddev-srrl-api-web

## Cache Layer configuration

In order to export content from the CMS to the Lumen-based API (also called Cache Layer since its purpose is to store flat, easy-to-access data to be consumed by the front-end) you'll need to configure the module. Go to /admin/config/services/srr_cache_connector

See https://bitbucket.org/matrushka/srrl-lumen-api/src/master/

NOTE: the description of the authentication token field is wrong! It says that the expected value is a JWT token. In reality, the value should simply be the APP_KEY from the API instance, found in the .env file. Using the key is far from ideal but since it's just a cache layer and the API is extremely simple and file-based, sharing the key is not a big threat.

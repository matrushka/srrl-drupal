<?php

$aliases['local'] = array(
  'root' => '/var/www/html/web',
  'uri' => 'https://srrl-database.ddev.site',
  'path-aliases' => array(
    '%files' => 'sites/default/files',
  ),
);

$aliases['live'] = array(
  'root' => '/home/master/applications/ememtztbsx/public_html/web',
  'uri' => 'https://laws.sexualrightsdatabase.org/',
  'remote-host' => '142.93.46.39',
  'remote-user' => 'master_khwxzfxhap',
  'path-aliases' => array(
    '%drush-script' => '/home/master/applications/ememtztbsx/public_html/vendor/drush/drush/drush',
    '%files' => 'sites/default/files'
  ),
);

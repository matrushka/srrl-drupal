# About current composer.json files

Dependencies in this project are not managed by composer, and is not required to run `composer install` to initialize the code.

The current `composer.json` file is present in case this project is installed on a web server that conflicts with `drush launcher`and drupal seven.

If you attempt to run any drush command and receive an error saying something like *The Drush launcher could not find a Drupal*, run `composer install` to install drush locally, and be able to manage this Drupal 7 site.
## SRRL Authentication Service

This module provides authentication services for the SRRL Cache layer. Content exporting and transfer to the Cache Layer are provided by _srrl\_cache\_connector_.

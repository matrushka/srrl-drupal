<?php

function srrl_services_get_user_by_email($email) {
  if(!function_exists('user_load_by_mail')) {
    module_load_include('module', 'user');
  }

  $account = user_load_by_mail($email);
  $user = srrl_prepare_user_data($account);
  return $user;
}

function srrl_services_authenticate_user($mail, $pass) {
  if(!function_exists('user_load_by_mail')) {
    module_load_include('module', 'user');
  }

  $account = user_load_by_mail($mail);

  $uid = user_authenticate($account->name, $pass);

  unset($account);
  $account = user_load($uid);

  $user_data =  srrl_prepare_user_data($account);

  if(!$user_data) {
    services_error('Authentication error', 401);
  }
  else {
    return $user_data;
  }
}


function srrl_prepare_user_data($account) {
  if(!$account) return false;

  $user = array(
    'uid' => $account->uid,
    'name' => $account->name,
    'mail' => $account->mail,
    'roles' => $account->roles
  );
  
  return $user;
}

function srrl_services_update_password($mail, $pass, $pass_confirm) {
  if(!function_exists('user_load_by_mail')) {
    module_load_include('module', 'user');
  }

  if($pass != $pass_confirm) {
    services_error('Passwords do not match', 400);
  }

  $account = user_load_by_mail($mail);

  $saved = user_save($account, array('pass' => $pass));

  if($saved) {
    return 'Password saved';
  }
  else {
    services_error('Error saving password');
  }
}

function srrl_services_access() {
  global $user;
  if($user) return true;
}

function srrl_services_authenticate_access() {
  return true;
}

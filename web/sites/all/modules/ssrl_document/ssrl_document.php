<?php
//filter.api.php
function hook_filter_info() {
   $filters['filter_document'] = array(
    'title' => t('Convert a href into document links'),
    'description' => t('Converts doc://[NID] into working URLs'),
    'process callback' => '_filter_document'
  );
}

//filter.module
function filter_filter_info() {
   $filters['filter_document'] = array(
    'title' => t('Convert a href tag into document links'),
    'process callback' => '_filter_document'
  );
}

//nid
function _filter_document($text)
{
  $newUrl = '';
  $n=0;

  $nidMatches;
  preg_match_all('/"doc:\/\/\d+"/', $text, $matches);
  foreach ($matches as $match) {
    foreach($match as $m)
    {
      preg_match_all('/\d+/', $m, $nid);
    //node ID = $nid[0][0];
      $id = $nid[0][0];
      $node = node_load($id);
      $url = $node->field_url[$node->language][0]['value'];
      if(!empty($node->field_file[$node->language][0]['filename']))
        $file = 'http://srrl.matrushka.com.mx/sites/default/files/documents/'.$node->field_file[$node->language][0]['filename'];
      if(!empty($url) && !empty($file))
        $newUrl = $url.'" data-url="'.$url.'" file_url="'.$file;
      else if(!empty($url) && empty($file))
        $newUrl = $url.'" data-url="'.$url.'"';
      $text =  preg_replace('/doc:\/\/'.$id.'/', $newUrl, $text);
    }
  }
  return $text;
}
?>


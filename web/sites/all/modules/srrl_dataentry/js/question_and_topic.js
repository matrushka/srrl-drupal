(function($) {
  $(function() {
    var $form = $('.node-form');

    // Focus the topic selection
    var $topicSelect = $('#s2id_edit-field-topic-und')
    $topicSelect.select2('focus');

    // Capture cmd+s and ctrl+s and simulate save and continue
    $(window).keypress(function(evt) {
      if(evt.charCode == 8747) {
        var $submit = $('#edit-submit-multiple');
        $submit.click();
        evt.preventDefault();
      }
    });

    $(window).keydown(function(evt) {
      if(evt.ctrlKey && !evt.altKey && !evt.metaKey && evt.charCode == 0) {
        rhythmCommand(evt.timeStamp);
      }
    });
  })
})(jQuery)

function rhythmCommand(timestamp) {
  if(typeof(rhythm) == 'undefined') {
    rhythm = [];
  }

  rhythm.push(timestamp);

  // If we have three or more taps, measure the time elapsed for the last three
  if(rhythm.length == 3) {
    var tapDuration = 0;
    for(var i=rhythm.length-1; i>=0; i--) {
      currentTimestamp = rhythm[i];

      if(i < rhythm.length-1) {
        tapDuration += rhythm[i+1] - rhythm[i];
      }
    }
    
    console.log(tapDuration);
    if(tapDuration < 400) {
      var $submit = jQuery('#edit-submit-multiple');
      $submit.click();
    }

    rhythm = [];
  }
}

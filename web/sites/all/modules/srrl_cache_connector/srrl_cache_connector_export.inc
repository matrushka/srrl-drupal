<?php

function scc_reset_export_timestamps() {
  db_delete('{srrl_export_timestamps}')->execute();
}

function scc_get_refresh_content_list($environment = 'test') {
  // Get list of countries that need refresh
  $countries = scc_get_refresh_countries($environment);

  // Get list of topics that need refresh
  $topics = scc_get_refresh_topics($environment);

  // Get list of pages that need refresh
  $pages = scc_get_refresh_pages($environment);

  return array(
    'countries' => $countries,
    'topics' => $topics,
    'pages' => $pages
  );
}

function scc_get_refresh_countries($environment = 'test') {
  module_load_include('inc', 'srrl_cache_connector', 'srrl_cache_connector_taxonomy');

  $vocabulary_name = variable_get('srrl_cache_countries_vocabulary');
  $countries = scc_get_vocabulary_tree_by_name($vocabulary_name);

  $need_refresh = array();
  foreach($countries as $country) {
    // Ignore not countries that are not published yet
    if(array_key_exists('published', $country) && $country['published'] == 0) {
      watchdog('srrl_cache_connector', 'Unpublished country not exported :country_name.', array(':country_name'=>$country['name']), WATCHDOG_WARNING);
      continue;
    }

    $refresh_timestamp = scc_content_needs_refresh('country', $country['tid'], $environment);
    if($refresh_timestamp !== FALSE) {
      $need_refresh[] = array('id' => $country['tid'], 'timestamp' => $refresh_timestamp);
    }
  }

  return $need_refresh;
}

function scc_get_refresh_topics($environment = 'test') {
  $vocabulary_name = variable_get('srrl_cache_topics_vocabulary');
  $topics = scc_get_vocabulary_tree_by_name($vocabulary_name);

  $vocabulary_name = variable_get('srrl_cache_countries_vocabulary');
  $countries = scc_get_vocabulary_tree_by_name($vocabulary_name);

  $need_refresh = array();
  foreach($countries as $country) {
    $country_tid = $country['tid'];
    if(array_key_exists('published', $country) && $country['published'] == 0) {
      continue;
    }

    foreach($topics as $topic) {
      // Skip the topics that aren't meant to be exported
      if(array_key_exists('not_covered_in_research', $topic) && $topic['not_covered_in_research']) {
        continue;
      }

      $topic_tid = $topic['tid'];
      $topic_nid = scc_get_country_topic_nid($topic_tid, $country_tid);

      if($topic_nid) {
        $refresh_timestamp = scc_content_needs_refresh('topic', $topic_nid, $environment);
        if($refresh_timestamp !== FALSE) {
          $need_refresh[] = array('id' => $topic_nid, 'timestamp' => $refresh_timestamp);
        }
      }
    }
  }

  return $need_refresh;
}

function scc_get_refresh_questions($environment = 'test') {
  $vocabulary_name = variable_get('srrl_cache_topics_vocabulary');
  $topics = scc_get_vocabulary_tree_by_name($vocabulary_name);

  $vocabulary_name = variable_get('srrl_cache_countries_vocabulary');
  $countries = scc_get_vocabulary_tree_by_name($vocabulary_name);

  $refresh_questions = array();
  foreach($countries as $country) {
    $country_tid = $country['tid'];

    foreach($topics as $topic) {

      // Iterate questions, we're not looking for topics
      foreach($topic['children'] as $question) {
        $question_tid = $question['tid'];
        $question_nid = scc_get_country_topic_nid($question_tid, $country_tid);

        if($question_nid) {
          if(scc_content_needs_refresh('question', $question_nid, $environment)) {
            $refresh_questions[] = $question_nid;
          }
        }

      }
    }
  }

  return $refresh_questions;
}

function scc_get_refresh_pages($environment) {
  // Just return all the existing page codes
  // it's so little content that it's just easier to export it all
  // than to check update times
  $sql = 'SELECT DISTINCT field_page_code_value FROM {field_data_field_page_code}';
  $result = db_query($sql);

  $codes = $result->fetchCol();

  $need_refresh = array();
  foreach($codes as $code) {
    $need_refresh[] = array('id' => $code, 'timestamp' => 0);
  }

  return $need_refresh;
}

function scc_set_content_export_timestamp($type, $id, $timestamp, $environment) {
  $sql_exists = "SELECT COUNT(*) FROM {srrl_export_timestamps} WHERE id = :id AND type = :type AND environment = :environment";
  $result = db_query($sql_exists, array(':type' => $type, ':id' => $id, ':environment' => $environment));
  $exists = $result->fetchField();

  $record = array(
    'id' => $id,
    'type' => $type,
    'last_update' => $timestamp,
    'last_export' => time(),
    'environment' => $environment
  );

  if($exists) {
    return drupal_write_record('srrl_export_timestamps', $record, array('id', 'type', 'environment'));
  }
  else {
    return drupal_write_record('srrl_export_timestamps', $record);
  }
}

function scc_content_needs_refresh($type, $id, $environment) {
  $last_exported_update = scc_get_content_last_exported_update($type, $id, $environment);
  $last_update = scc_get_content_last_update($type, $id);

  return ($last_update > $last_exported_update) ? $last_update : FALSE;
}

function scc_get_content_last_exported_update($type, $id, $environment) {
  $sql = "SELECT last_update FROM {srrl_export_timestamps} WHERE type = :type AND id = :id AND environment = :environment";
  $result = db_query($sql, array(':type' => $type, ':id' => $id, ':environment' => $environment));

  if($result->rowCount() == 1) {
    return $result->fetchField();
  }
  else {
    return 0;
  }

}

function scc_get_content_last_update($type, $id) {
  switch($type) {
    case 'country':
      // Get latest update timestamp from question and topic child elements
      $sql = <<<SQL
SELECT c.entity_id as nid, c.bundle as type, n.changed as changed FROM
  {field_data_field_country} AS c
  LEFT JOIN {node} AS n
  ON c.entity_id = n.nid
WHERE c.field_country_tid = :country_tid
AND c.bundle IN ('topic_country', 'question_country')
ORDER BY n.changed DESC
SQL;
      $result = db_query($sql, array(':country_tid' => $id));

      // If there is no content for this country
      // then it hasn't been updated, ever
      if($result->rowCount() == 0) {
        return 0;
      }

      $children_update_timestamps = $result->fetchAll();
      $children_last_update = $children_update_timestamps[0]->changed;

      $get_nid = function($obj) {
        return $obj->nid;
      };

      $children_nids = array_map($get_nid, $children_update_timestamps);

      // Get the latest document update timestamp from the related questions
      $sql = <<<SQL
SELECT n.changed as changed FROM
{field_data_field_related_documents} AS rd
    LEFT JOIN {node} AS n
    ON rd.field_related_documents_target_id = n.nid
WHERE rd.entity_id IN(:children_nids)
ORDER BY n.changed DESC
LIMIT 1;
SQL;

      $result = db_query($sql, array(':children_nids' => $children_nids));
      $doc_last_update = $result->fetchField();
      $last_update = max($doc_last_update, $children_last_update);

      return $last_update;

      break;
    default:
      $sql          = 'SELECT n.changed as changed FROM {node} as n WHERE n.nid = :nid';
      $result       = db_query($sql, array(':nid' => $id));
      $last_update  = $result->fetchField();

      return $last_update;

      break;
  }
}

function scc_get_question_by_taxonomy($topic_tid, $country_tid) {
  return scc_get_content_by_taxonomy($topic_tid, $country_tid, 'question_country');
}

function scc_get_topic_by_taxonomy($topic_tid, $country_tid) {
  return scc_get_content_by_taxonomy($topic_tid, $country_tid, 'topic_country');
}

function scc_get_content_by_taxonomy($topic_tid, $country_tid, $content_type) {
  $nid = scc_get_country_topic_nid($topic_tid, $country_tid);

  if($nid) {
    // We need to check if the content is in fact a question
    $node = node_load($nid);
    if($node->type == $content_type) {
      return scc_get_question($nid);
    }
    else {
      return NULL;
    }
  }
}

function scc_get_question($id) {
  $node = node_load($id);

  $meta = array();
  $doc  = array();

  $meta['nid'] = $id;
  $meta['changed'] = $node->changed;

  // Get the topic term details and assign them to meta and doc
  $topic_term = taxonomy_term_load($node->field_topic['und'][0]['tid']);
  $meta['topic_id'] = $topic_term->tid;
  $doc['topic'] = array('tid' => $topic_term->tid, 'name' => $topic_term->name);

  // Get the country term details and assign them to meta and doc
  $country_term = taxonomy_term_load($node->field_country['und'][0]['tid']);
  $meta['country_id'] = $country_term->tid;
  $doc['country'] = array('tid' => $country_term->tid, 'name' => $country_term->name);

  // Get the answer and add it to doc
  $answer = $node->field_answer['und'][0]['safe_value'];
  $doc['answer'] = $answer;

  // Get the explanation and add it to doc
  if(!empty($node->field_explanation)) {
    $explanation = $node->field_explanation['und'][0]['safe_value'];
    $doc['explanation'] = $explanation;
  }

  // Get the related documents and add them to doc
  if(!empty($node->field_related_documents)) {
    $get_document_nid = function($item) {
      return $item['target_id'];
    };
    $document_nids = array_map($get_document_nid, $node->field_related_documents['und']);

    $documents = array();
    foreach($document_nids as $document_nid) {
      $document_node = node_load($document_nid);

      $document_content = array(
        'title' => $document_node->title
      );

      if(!empty($document_node->field_file)) {
        $document_content['file_url'] = file_create_url($document_node->field_file['und'][0]['uri']);
      }

      if(!empty($document_node->field_url)) {
        $document_content['url'] = $document_node->field_url['und'][0]['value'];
      }

      $documents[] = $document_content;
    }

    $doc['related_documents'] = $documents;
  }

  return array_merge($meta, array('doc' => $doc));
}

function scc_get_topic($id) {
  module_load_include('inc', 'srrl_cache_connector', 'srrl_cache_connector_taxonomy');

  $node = node_load($id);

  $meta = array();
  $doc  = array();

  $meta['nid'] = $id;
  $meta['changed'] = $node->changed;

  // Get the topic term details and assign them to meta and doc
  $topic_term = taxonomy_term_load($node->field_topic['und'][0]['tid']);
  $meta['topic_id'] = $topic_term->tid;
  $doc['topic'] = array('tid' => $topic_term->tid, 'name' => $topic_term->name);

  // Get the country term details and assign them to meta and doc
  $country_term = taxonomy_term_load($node->field_country['und'][0]['tid']);
  $meta['country_id'] = $country_term->tid;
  $doc['country'] = array('tid' => $country_term->tid, 'name' => $country_term->name);

  $evaluation_code_tid = $node->field_evaluation_code['und'][0]['tid'];
  $evaluation_code = taxonomy_term_load($evaluation_code_tid);
  $evaluation_color = $evaluation_code->field_color['und'][0]['rgb'];

  $evaluation = array(
    'label' => $evaluation_code->name,
    'color' => $evaluation_color
  );

  $doc['evaluation'] = $evaluation;

  if(!empty($node->field_topic_tags)) {
    $get_tid = function($item) {
      return $item['tid'];
    };

    $topic_tags_tids = array_map($get_tid, $node->field_topic_tags['und']);
    $topic_tag_terms = taxonomy_term_load_multiple($topic_tags_tids);

    $topic_tags = array();
    foreach($topic_tag_terms as $key => $tag_term) {
      $tag = array(
        'label' => $tag_term->name,
        'code' => $tag_term->field_code['und'][0]['safe_value'],
        'tid' => $tag_term->tid,
        'icon' => (empty($tag_term->field_icon['und'])) ? '' : $tag_term->field_icon['und'][0]['safe_value']
      );

      $topic_tags[] = $tag;
    }

    $doc['tags'] = $topic_tags;
  }

  return array_merge($meta, array('doc' => $doc));
}

function scc_get_country_topic_nid($topic_tid, $country_tid) {
  $sql = <<<SQL
SELECT n.nid FROM
  {node} as n
  LEFT JOIN {field_data_field_country} as c
    ON c.entity_id = n.nid
  LEFT JOIN {field_data_field_topic} as t
    ON t.entity_id = n.nid
WHERE c.field_country_tid = :country_tid
AND t.field_topic_tid = :topic_tid
SQL;

  $result = db_query($sql, array(':country_tid' => $country_tid, ':topic_tid' => $topic_tid));
  $rows = $result->rowCount();
  $column = $result->fetchCol();
  $topic_term = taxonomy_term_load($topic_tid);
  $topic_parents = taxonomy_get_parents($topic_tid);

  if(!empty($topic_parents)) {
    $parent_tid = array_keys($topic_parents)[0];
    $parent_topic = $topic_parents[$parent_tid];
    $topic_name = $parent_topic->name . ' >> ' . $topic_term->name;
    $is_question = true;
  }
  else {
    $topic_name = $topic_term->name;
    $is_question = false;
  }
  $topic_name = '<em>' . $topic_name . '</em>';

  $country_term = taxonomy_term_load($country_tid);

  if($rows == 1) {
    return $column[0];
  }
  else if($rows > 1) {
    $links = array();
    foreach($column as $nid) {
      $duplicate_node = node_load($nid);
      $duplicate_type = $duplicate_node->type;
      $destination = (arg(0) == 'preview') ? '?destination='.current_path() : '';
      $links[] = '<li><a href="/node/'.$nid.'">'.$duplicate_type.' '.$nid.'</a> <a href="/node/'.$nid.'/edit'.$destination.'">(edit)</a></li>';
    }
    $links = implode('',$links);
    drupal_set_message('Warning: multiple entries for combination of country ' . $country_tid . ' and topic ' . $topic_tid . ' ('.$topic_name.'). The nodes in conflict are: <ul>'.$links.'</ul>');
    watchdog('srrl_cache_connector', 'Warning: two country topic nodes found for country ' . $country_tid . ' and topic ' . $topic_tid . ' (nids %nids)', array('%nids' => implode(', ', $column)), WATCHDOG_WARNING);

    return $column[0];
  }
  else {
    $create_path = ($is_question) ? 'node/add/question-country' : 'node/add/topic-country';
    $url_options = array('query' => array('country' => $country_term->name, 'topic_id' => $topic_term->tid));
    $create_url = url($create_path, $url_options);
    $create_link = l('Create', $create_path, $url_options);
    drupal_set_message('Missing topic or question in '.$country_term->name.': ' . $topic_name . '. ' . $create_link);
    return NULL;
  }
}

function scc_get_country($id) {
  module_load_include('inc', 'srrl_cache_connector', 'srrl_cache_connector_taxonomy');

  $meta = array();
  $doc = array();

  $meta['country_id'] = $id;
  $meta['changed'] = 0; // This will be changed later to reflect the latest child updated

  // Get the country taxonomy term
  $country_term = taxonomy_term_load($id);
  $doc['country'] = array('tid' => $country_term->tid, 'name' => $country_term->name);

  // Get the topics (and questions) tree
  $topics_vocabulary_name = variable_get('srrl_cache_topics_vocabulary');
  $topics_tree = scc_get_vocabulary_tree_by_name($topics_vocabulary_name);

  // Populate the topics tree with topic documents for the first level
  // and question documents for the second
  foreach($topics_tree as $index => &$topic) {
    if(array_key_exists('not_covered_in_research', $topic) && $topic['not_covered_in_research']) {
      unset($topics_tree[$index]);
      continue;
    }

    // Move the tid to the key topic_id for consistency
    $topic['topic_id'] = $topic['tid'];
    unset($topic['tid']);

    // Find out if there is a node for this topic and country
    $topic_country_nid = scc_get_country_topic_nid($topic['topic_id'], $id);

    // If there is, load the content
    if(!empty($topic_country_nid)) {
      $topic_content = scc_get_topic($topic_country_nid);
      $topic = array_merge($topic, $topic_content);

      // Update the timestamp with the topic timestamp if newer
      $meta['changed'] = max($meta['changed'], $topic_content['changed']);

      // Rename children to questions
      $topic['questions'] = $topic['children'];
      unset($topic['children']);

      // Load all questions that exist into an array
      $questions = array();
      foreach($topic['questions'] as $question) {
        $question_country_nid = scc_get_country_topic_nid($question['tid'], $id);

        if(!empty($question_country_nid)) {
          $question = scc_get_question($question_country_nid);
          $questions[] = $question;

          // Update the timestamp with the question timestamp if newer
          $meta['changed'] = max($meta['changed'], $question['changed']);
        }
      }

      // If any question content was found, add the questions array
      // to the topic content
      if(count($questions) > 0) {
        $topic['questions'] = $questions;
      }
      else {
        $topic['questions'] = NULL;
      }
    }
  }

  $doc['children'] = array_values($topics_tree);

  return array_merge($meta, array('doc' => $doc));
}

function scc_get_page($page_code) {
  // Find the nid from the page code
  $sql = 'SELECT entity_id FROM {field_data_field_page_code} WHERE field_page_code_value=:page_code';
  $result = db_query($sql, array(':page_code' => $page_code));

  $nids = $result->fetchCol();

  $numPagesFound = count($nids);

  if($numPagesFound == 0) {
    return false;
  }
  else if($numPagesFound == 1) {
    $nid = $nids[0];
  }
  else {
    $nid = $nids[0];
    watchdog('srrl_cache_connector', 'There are more than one pages with the code :page_code (:nids).', array(':page_code'=>$page_code, ':nids' => $nids, WATCHDOG_WARNING));
  }

  $page_node = node_load($nid);

  $page = array();
  $page['id'] = $page_code;
  $page['title'] = $page_node->title;
  $page['contents'] = $page_node->body['und'][0]['safe_value'];
  $page['code'] = $page_code;

  return $page;
}

function scc_send_country_to_cache($country_id, $environment = 'test') {
  // Get the country data
  $country = scc_get_country($country_id);
  $id = $environment . '-country-' . $country_id;


  $fields = array(
    'id' => $id,
    'environment' => $environment,
    'country_id' => $country_id,
    'contents' => json_encode($country)
  );

  $endpoint = variable_get('srrl_cache_country_endpoint');

  return scc_post_to_cache_api($endpoint, $fields);
}

function scc_send_topic_to_cache($topic_nid, $environment='test') {
  $topic = scc_get_topic($topic_nid);
  $country_id = $topic['country_id'];
  $topic_id = $topic['topic_id'];
  $id = $environment . '-topic-' . $country_id . '-' . $topic_id;

  $fields = array(
    'id' => $id,
    'country_id' => $country_id,
    'topic_id' => $topic_id,
    'contents' => json_encode($topic),
    'environment' => $environment
  );

  $endpoint = variable_get('srrl_cache_topic_country_endpoint');

  return scc_post_to_cache_api($endpoint, $fields);
}

function scc_send_taxonomy_to_cache($vocabulary_name, $environment='test') {
  module_load_include('inc', 'srrl_cache_connector', 'srrl_cache_connector_taxonomy');
  $id = $environment . '-taxonomy-' . $vocabulary_name;
  $tree = scc_get_vocabulary_tree_by_name($vocabulary_name);

  $fields = array(
    'id' => $id,
    'taxonomy_name' => $vocabulary_name,
    'environment' => $environment,
    'contents' => json_encode($tree)
  );

  $endpoint = variable_get('srrl_cache_taxonomy_endpoint');

  return scc_post_to_cache_api($endpoint, $fields);
}

function scc_send_page_to_cache($page_code, $environment='test') {
  $page = scc_get_page($page_code);

  $contents = array(
    'title' => $page['title'],
    'body' => $page['contents']
  );

  $fields = array(
    'id' => $page['id'],
    'code' => $page['code'],
    'contents' => json_encode($contents)
  );

  $endpoint = variable_get('srrl_cache_page_endpoint');

  return scc_post_to_cache_api($endpoint, $fields);
}

function scc_post_to_cache_api($endpoint, $fields) {
  $token = variable_get('srrl_cache_token', false);

  if(!$token) {
    return false;
  }

  // Delete from cache api first to avoid unique id validation errors
  scc_delete_from_cache_api($fields['id'], $endpoint);

  $host = variable_get('srrl_cache_host');
  // $url = 'http://' . $host . $endpoint;
  $url = $host . $endpoint;

  $formatted_fields = http_build_query($fields);

  static $ch;
  if(!$ch) {
    $ch = curl_init();
  }

  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_POST, count($fields));
  curl_setopt($ch, CURLOPT_POSTFIELDS, $formatted_fields);
  // curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: ' . $token));
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

  $response = curl_exec($ch);
  $error = curl_error($ch);
  $info = curl_getinfo($ch);

  if($error || !in_array($info['http_code'], array('200', '201'))) {
    $error = (empty($error)) ? 'Error sending content to the cache layer' : $error;
    $message = $error . ' (http code ' . $info['http_code'] . ')';
    drupal_set_message($message);
    watchdog('srrl_cache_connector', $message, array(), WATCHDOG_ERROR);
    return false;
  }
  else {
    return true;
  }
}

function scc_delete_from_cache_api($id, $endpoint) {
  $host = variable_get('srrl_cache_host');
  // $url = 'http://' . $host . $endpoint . '/' . $id;
  $url = $host . $endpoint . '/' . $id;

  $token = variable_get('srrl_cache_token', false);

  if(!$token) {
    return false;
  }

  static $ch;
  if(!$ch) {
    $ch = curl_init();
  }

  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
  // curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: ' . $token));
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

  $response = curl_exec($ch);
  $error = curl_error($ch);

  if($error) {
    drupal_set_message($error);
    watchdog('srrl_cache_connector', $error);
    return false;
  }
  else {
    return true;
  }

  return $response;
}

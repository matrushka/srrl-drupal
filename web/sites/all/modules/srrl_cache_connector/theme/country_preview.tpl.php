<div class="country-container">
<h2 class="country-name"><?php print $country['doc']['country']['name']; ?></h2>
<a href="/preview/countries-list" class="cms-preview">List of countries</a><br>
<?php

// This is the country preview template file

// The $country variable has an object with all the country data,
// including topics and questions
//window.location.hash = this.value
?>

<p>Select a topic: <select style="font-size: 12;" name="jump-menu" onchange="window.location.hash = this.value">
<?php
// There are a couple more files to theme topics and questions.
// Here is a brief example that could help
foreach($country['doc']['children'] as $topic){?>
  <option value="<?php print $topic['name'];?>"><?php print $topic['name']; ?></option>
<?php } ?>
</select>
<button class="global-explanation-button cms-preview">Toggle all explanations</button>
</p>

<?php
foreach($country['doc']['children'] as $topic) {
  print theme('topic_preview', array('topic' => $topic));
}
?>
</div>

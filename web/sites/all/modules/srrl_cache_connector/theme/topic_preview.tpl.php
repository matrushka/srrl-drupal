<h2 class="topic-title" id="<?php print $topic['name']; ?>"><?php print $topic['name']; ?> <a class="cms-preview small-text" href="/node/<?php print $topic['nid']; ?>/edit?destination=<?php print current_path(); ?>">edit topic</a></h2>
<p><b>Evaluation code: <em><?php print $topic['doc']['evaluation']['label'];?></em></b></p>
<?php
  if(array_key_exists('tags', $topic['doc']) && !empty($topic['doc']['tags'])) {
    print "<p>Topic tags:</p><ul>";

    foreach ($topic['doc']['tags'] as $tag) {
      $label = $tag['label'];
      print "<li>$label</li>";
    }

    print "</ul>";
  }
?>
<div class="topic-questions">
<?php
// $topic contains all the topic data, including child questions
if(!empty($topic['questions'])) 
{
  foreach($topic['questions'] as $question) 
    print theme('question_preview', array('question' => $question, 'topic' => $topic));
}
?>
</div>

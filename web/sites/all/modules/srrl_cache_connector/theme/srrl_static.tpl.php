<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title><?php print $page_title; ?></title>
  <link rel="stylesheet" href="static-styles.css">
  <link rel="stylesheet" href="country_preview_style.css">
</head>
<body>
<div class="header">
  <img id="logo" src="sri-logo.png">
  <h1>National Sexual Rights Law and Policy Database</h1>
</div>
<div class="main-container">
  <div class="left-sidebar">
    <ul class="menu">
      <li><a href="page-welcome-static.html">Welcome</a></li>
      <li><a href="page-about.html">About</a></li>
      <li><a href="page-related_resources.html">Related Resources</a></li>
      <li><a href="page-acknowledgements.html">Acknowledgements</a></li>
      <li><a href="countries-list.html">Countries</a></li>
    </ul>
  </div>
  <div class="content">
    <?php print $page_content; ?> 
  </div>
</div>
</body>
</html>

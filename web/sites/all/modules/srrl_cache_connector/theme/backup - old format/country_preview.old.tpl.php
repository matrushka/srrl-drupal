<?php
global $user;
if($user->uid == 1) {
  include('country_preview.new.tpl.php');
}
else {
?>
<div class="country-container">
<a href="/preview/countries-list">List of countries</a><br>
<?php

// This is the country preview template file

// The $country variable has an object with all the country data,
// including topics and questions
//window.location.hash = this.value
?>

<p>Select a topic: <select style="font-size: 12;" name="forma" onchange="window.location.hash = this.value"></p>
<?php
// There are a couple more files to theme topics and questions.
// Here is a brief example that could help
foreach($country['doc']['children'] as $topic){?>
  <option value="<?php print $topic['name'];?>"><?php print $topic['name']; ?></option>
  <?php } ?>
</select>

<?php
foreach($country['doc']['children'] as $topic) {
  print theme('topic_preview', array('topic' => $topic));
}
?>
</div>
<?php
}
?>

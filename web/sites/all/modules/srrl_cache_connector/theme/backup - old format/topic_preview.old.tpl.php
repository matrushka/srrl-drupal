<?php
global $user;
if($user->uid == 1) {
  include('topic_preview.new.tpl.php');
}
else {
?>
<ul><h2 id="<?php print $topic['name']; ?>"><?php print $topic['name']; ?></h2>
<table>
  <tr>
    <td><b>Code</b></td>
    <td><b><?php print $topic['doc']['evaluation']['label'];?></b></td>
  </tr>

<?php
// $topic contains all the topic data, including child questions
if(!empty($topic['questions'])) 
{
  foreach($topic['questions'] as $question) 
    print theme('question_preview', array('question' => $question, 'topic' => $topic));
}
?>
</table>
</ul>
<?php
}
?>

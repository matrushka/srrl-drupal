<?php
global $user;
if($user->uid == 1) {
  include('question_preview.new.tpl.php');
}
else {
?>
<tr>
  <td colspan="2"><b><?php print $question['doc']['topic']['name']; ?></b></td>
</tr>
<tr>
  <td colspan="2">
  <?php
    if(!empty($question['doc']['answer']))
      print $question['doc']['answer'];
    if(!empty($question['doc']['explanation']))
      print '<button class="explanation-button">Explanation</button>';
    print '<div class="explanation-container">';
    if(!empty($question['doc']['explanation']))
      print "<br><br>".$question['doc']['explanation'];
    print '</div>';
  ?>
  </td>
</tr>
<?php
}
?>

<div class="question">
  <h3 class="question-title"><?php print $question['doc']['topic']['name']; ?><a class="cms-preview small-text" href="/node/<?php print $question['nid']; ?>/edit?destination=<?php print current_path(); ?>">edit question</a></h3>
  <?php
    if(!empty($question['doc']['answer']))
      print $question['doc']['answer'];
    if(!empty($question['doc']['explanation']))
      print '<button class="explanation-button cms-preview">Toggle explanation</button>';
    print '<div class="explanation-container">';
    if(!empty($question['doc']['explanation']))
      print $question['doc']['explanation'];
    print '</div>';
  ?>
</div>

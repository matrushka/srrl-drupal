<?php

function srrl_cache_connector_admin() {
  $form = array();

  $form['srrl_cache_config'] = array(
    '#type' => 'fieldset',
    '#title' => 'SRRL Cache layer configuration',
    '#weight' => '1',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE
  );

  $form['srrl_cache_config']['srrl_cache_host'] = array(
    '#type' => 'textfield',
    '#title' => t('Cache layer host'),
    '#default_value' => variable_get('srrl_cache_host', 'localhost'),
    '#maxlength' => 150,
    '#description' => t("Please include http:// or https://. Defaults to <em>localhost</em>"),
    '#required' => TRUE,
  );

  $form['srrl_cache_config']['srrl_cache_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Cache layer authentication token'),
    '#description' => 'JWT token obtained from the cache layer (Sails JS)',
    '#default_value' => variable_get('srrl_cache_token', ''),
    '#maxlength' => 500,
    '#required' => TRUE,
  );

  $form['srrl_cache_config']['srrl_cache_taxonomy_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Taxonomy lists and trees endpoint'),
    '#default_value' => variable_get('srrl_cache_taxonomy_endpoint', '/api/taxonomy'),
    '#maxlength' => 150,
    '#description' => t("Example: <em>/api/taxonomy</em>"),
    '#required' => TRUE,
  );

  $form['srrl_cache_config']['srrl_cache_topic_country_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Topic-Countries endpoint'),
    '#default_value' => variable_get('srrl_cache_topic_country_endpoint', '/api/topic-country'),
    '#maxlength' => 150,
    '#required' => TRUE,
  );

  $form['srrl_cache_config']['srrl_cache_country_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Countries endpoint'),
    '#default_value' => variable_get('srrl_cache_country_endpoint', '/api/country'),
    '#maxlength' => 150,
    '#required' => TRUE,
  );

  $form['srrl_cache_config']['srrl_cache_page_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Pages endpoint'),
    '#default_value' => variable_get('srrl_cache_page_endpoint', '/api/page'),
    '#maxlength' => 150,
    '#required' => TRUE,
  );

  $form['srrl_cache_config']['srrl_cache_batch_max_items'] = array(
    '#type' => 'textfield',
    '#title' => t('Max items to export in each batch pass'),
    '#default_value' => variable_get('srrl_cache_batch_max_items', '5'),
    '#maxlength' => 3,
    '#required' => TRUE,
  );

  $form['srrl_cache_config']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save cache configuration')
  );

  $form['srrl_taxonomy_config'] = array(
    '#type' => 'fieldset',
    '#title' => 'srrl Taxonomy configuration',
    '#weight' => '2',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE
  );

  $form['srrl_taxonomy_config']['srrl_cache_countries_vocabulary'] = array(
    '#type' => 'textfield',
    '#title' => t('Countries vocabulary'),
    '#default_value' => variable_get('srrl_cache_countries_vocabulary', 'countries'),
    '#maxlength' => 150,
    '#required' => TRUE,
  );

  $form['srrl_taxonomy_config']['srrl_cache_topics_vocabulary'] = array(
    '#type' => 'textfield',
    '#title' => t('Topics vocabulary'),
    '#default_value' => variable_get('srrl_cache_topics_vocabulary', 'topics'),
    '#maxlength' => 150,
    '#required' => TRUE,
  );

  $form['srrl_taxonomy_config']['srrl_cache_evaluation_codes_vocabulary'] = array(
    '#type' => 'textfield',
    '#title' => t('Evaluation codes vocabulary'),
    '#description' => 'Evaluation codes are the values assigned to each topic for a specific country. The evaluation code will determine how the country is presented in the map when a topic is visualised.',
    '#default_value' => variable_get('srrl_cache_evaluation_codes_vocabulary', 'evaluation_codes'),
    '#maxlength' => 150,
    '#required' => TRUE,
  );

  $form['srrl_taxonomy_config']['srrl_cache_topic_tags_vocabulary'] = array(
    '#type' => 'textfield',
    '#title' => t('Topic tags vocabulary'),
    '#description' => 'Topic tags are exceptions and other tags that apply for a topic in a certain country, or example the exception of <em>marriage</em> in sexual age of consent.',
    '#default_value' => variable_get('srrl_cache_topic_tags_vocabulary', 'topic_tags'),
    '#maxlength' => 150,
    '#required' => TRUE,
  );

  $form['srrl_taxonomy_config']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save taxonomy configuration')
  );

  $form['srrl_static_config'] = array(
    '#type' => 'fieldset',
    '#title' => 'SRRL Static file export configuration',
    '#weight' => '3',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE
  );

  $form['srrl_static_config']['srrl_static_dir'] = array(
    '#type' => 'textfield',
    '#title' => t('Static files directory'),
    '#default_value' => variable_get('srrl_static_dir', FALSE),
    '#maxlength' => 150,
    '#description' => t("Full filesystem path to the export directory. Defaults to FALSE, must be set to export."),
    '#required' => TRUE,
  );

  $form['srrl_static_config']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save static files configuration')
  );

  return system_settings_form($form);
}

function scc_export_form() {
  $form['select_types'] = array(
    '#type' => 'fieldset',
    '#title' => 'Select types',
    '#weight' => 0,
    '#collapsible' => false
  );

  $form['select_types']['export_countries'] = array(
    '#type' => 'checkbox',
    '#title' => 'countries',
    '#default_value' => 1,
    '#description' => 'will export countries with all their topics and questions'
  );

  $form['select_types']['export_topics'] = array(
    '#type' => 'checkbox',
    '#title' => 'Topics',
    '#default_value' => 1,
    '#description' => 'Will export topics as needed for map visualisation'
  );

  $form['select_types']['export_pages'] = array(
    '#type' => 'checkbox',
    '#title' => 'Pages',
    '#default_value' => 1,
    '#description' => 'Will export pages'
  );

  $form['select_destinations'] = array(
    '#type' => 'fieldset',
    '#title' => 'Select destinations',
    '#weight' => 0,
    '#collapsible' => false
  );

  $form['select_destinations']['to_sails'] = array(
    '#type' => 'checkbox',
    '#title' => 'Sails JS cache layer',
    '#default_value' => 1,
    '#description' => 'Will export to the cache layer for viewing through the web app'
  );

  $form['select_destinations']['to_static'] = array(
    '#type' => 'checkbox',
    '#title' => 'Static files',
    '#default_value' => 1,
    '#description' => 'Will export to static files as screen reader friendly content'
  );

  $form['select_environment'] = array(
    '#type' => 'fieldset',
    '#title' => 'Select environment',
    '#weight' => 1,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );

  $form['select_environment']['environment'] = array(
    '#type' => 'radios',
    '#title' => 'Environment',
    '#default_value' => 'test',
    '#options' => array('test' => 'Test', 'live' => 'Live'),
    '#description' => 'Choose <em>test</em> if you want to preview the exported data in the preview client. Choose <em>live</em> to export the data for end users.'
  );

  $form['select_taxonomies'] = array(
    '#type' => 'fieldset',
    '#title' => 'Export taxonomies',
    '#weight' => 3,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE
  );

  $form['select_taxonomies']['export_taxonomy_countries'] = array(
    '#type' => 'checkbox',
    '#title' => 'Countries taxonomy',
    '#description' => 'Will export the topics and questions tree. Check this only if countries were added or removed from the countries vocabulary.'
  );

  $form['select_taxonomies']['export_taxonomy_topics'] = array(
    '#type' => 'checkbox',
    '#title' => 'Topics taxonomy',
    '#description' => 'Will export the topics and questions tree. Check this only if the topics and questions taxonomy vocabulary was changed.'
  );

  $form['select_taxonomies']['export_taxonomy_evaluation_codes'] = array(
    '#type' => 'checkbox',
    '#title' => 'Evaluation codes taxonomy',
    '#description' => 'Will export the evaluation codes. Check this only if the evaluation codes taxonomy vocabulary was changed.'
  );

  $form['select_wipe'] = array(
    '#type' => 'fieldset',
    '#title' => 'Force update',
    '#weight' => 3,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE
  );

  $form['select_wipe']['force_update_all'] = array(
    '#type' => 'checkbox',
    '#title' => 'Update all content regardless of last update timestamp'
  );

  $form['submit'] = array('#type' => 'submit', '#value' => t( 'Export' ), '#weight' => 100);


  return $form;
}

function scc_export_form_submit($form, &$form_state) {
  module_load_include('inc', 'srrl_cache_connector', 'srrl_cache_connector_batch');
  scc_batch_export($form_state['values']);
}


(function($) {
  $(function() {
    $('.global-explanation-button').click(function() {
      var $button = $(this);
      $button.toggleClass('active');
      if($button.hasClass('active')) {
        $('.explanation-container').addClass('active');
      }
      else {
        $('.explanation-container').removeClass('active');
      }
    });

    $('.explanation-button').click(function() {
      var $explanation = $(this).parent().find('.explanation-container');
      $explanation.toggleClass('active');
    });

    $(window).on("hashchange", function () {
      window.scrollTo(window.scrollX, window.scrollY - 50);
    });
  })


})(jQuery)


<?php

function scc_test_page() {
  module_load_include('inc', 'srrl_cache_connector', 'srrl_cache_connector_static');
  //scc_static_countries_list('test');
  //scc_static_assets('test');
  scc_save_country_static(369, 'test');

  return 'Ran tests in scc_test_page, srrl_cache_connector_pages.inc';
}

function scc_export_countries_page() {
  $vocabulary_name = variable_get('srrl_cache_countries_vocabulary');
  return scc_render_taxonomy_export($vocabulary_name);
}

function scc_export_topics_page() {
  $vocabulary_name = variable_get('srrl_cache_topics_vocabulary');
  return scc_render_taxonomy_export($vocabulary_name);
}

function scc_render_taxonomy_export($vocabulary_name) {
  module_load_include('inc', 'srrl_cache_connector', 'srrl_cache_connector_taxonomy');

  if(!$vocabulary_name) {
    drupal_set_message('The ' . $vocabulary_name . ' vocabulary name is not defined yet', 'error');
    return '';
  }

  $tree = scc_get_vocabulary_tree_by_name($vocabulary_name);

  $output = '<textarea class="exported-json" style="width: 100%; border: 1px solid #333;" rows="25">';
  $output .= json_encode($tree);
  $output .= '</textarea>';

  return $output;
  
}

function scc_countries_list() {
  module_load_include('inc', 'srrl_cache_connector', 'srrl_cache_connector_taxonomy');
  
  $vocabulary_name = variable_get('srrl_cache_countries_vocabulary');
  $tree = scc_get_vocabulary_tree_by_name($vocabulary_name);
  
  $items = array();

  foreach($tree as $country) {
    $items[] = l($country[ 'name' ], 'preview/country/' . $country[ 'tid' ]);
  }

  return theme('item_list', array('items' => $items));
}

function scc_preview_country($tid = NULL) {
  module_load_include('inc', 'srrl_cache_connector', 'srrl_cache_connector_export');

  global $base_path;
  $module_path = drupal_get_path('module', 'srrl_cache_connector');
  drupal_add_js($module_path . '/js/country_preview.js');
  drupal_add_css($module_path . '/css/country_preview_style.css');

  if($tid == NULL) {
    drupal_not_found();
    return;
  }

  $country_term = taxonomy_term_load($tid);

  if($country_term == false) {
    return drupal_not_found();
  }

  $vocabulary = taxonomy_vocabulary_load($country_term->vid);
  $vocabulary_name = variable_get('srrl_cache_countries_vocabulary');

  if($vocabulary->machine_name != $vocabulary_name) {
    return drupal_not_found();
  }

  $country = scc_get_country($tid);
  return theme('country_preview', array('country' => $country));
}

function scc_preview_country_title($tid) {
  if(empty($tid)) {
    $tid = arg(2);

    if(empty($tid)) {
      return 'Country preview';
    }
  }

  $country_term = taxonomy_term_load($tid);
  return $country_term->name . ' - preview';
}

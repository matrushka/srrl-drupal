<?php

function scc_batch_export($config) {
  module_load_include('inc', 'srrl_cache_connector', 'srrl_cache_connector_export');
  module_load_include('inc', 'srrl_cache_connector', 'srrl_cache_connector_static');

  $environment = $config['environment'];

  if($config['force_update_all']) {
    scc_reset_export_timestamps();
  }

  // Get lists of content that need updating
  $update = scc_get_refresh_content_list($environment);

  $batch = array(
    'operations' => array(),
    'finished' => 'scc_batch_finished',
    'title' => 'Exporting SSRL content',
    'init_message' => 'Exporting started',
    'progress_message' => t('Exported @current out of @total'),
    'error_message' => t('There was an error while exporting the contents. Please contact the developer.'),
    'file' => drupal_get_path('module', 'srrl_cache_connector') . '/srrl_cache_connector_batch.inc'
  );

  // Build taxonomy exporting operations
  $taxonomies_to_export = array();
  if($config['export_taxonomy_countries']) {
    $taxonomies_to_export[] = array('id' => 'countries', 'timestamp' => time());
  }
  if($config['export_taxonomy_topics']) {
    $taxonomies_to_export[] = array('id' => 'topics', 'timestamp' => time());
  }
  if($config['export_taxonomy_evaluation_codes']) {
    $taxonomies_to_export[] = array('id' => 'evaluation_codes', 'timestamp' => time());
  }


  // Add only the operations that have items to export
  if(count($update['countries']) && $config['export_countries']) {
    $batch['operations'][] = array('scc_batch_export_countries', array( $update['countries'], $environment, $config ));
  }
  if(count($update['topics']) && $config['export_topics']) {
    $batch['operations'][] = array('scc_batch_export_topics', array( $update['topics'], $environment, $config ));
  }
  if(count($update['pages']) && $config['export_pages']) {
    $batch['operations'][] = array('scc_batch_export_pages', array( $update['pages'], $environment, $config ));
  }
  if(count($taxonomies_to_export)) {
    $batch['operations'][] = array('scc_batch_export_taxonomies', array($taxonomies_to_export, $environment, $config ));
  }

  // If pages or countries are to be exported and static files are enabled
  // prepare the environment for them
  if((count($update['pages']) && $config['export_pages']) && (count($update['countries']) && $config['export_countries']) && $config['to_static']) {
    scc_static_assets($environment);
    scc_static_countries_list($environment);
  }

  batch_set($batch);
}

function scc_batch_export_countries($countries, $environment, $config, &$context) {
  scc_batch_export_content('country', $countries, $environment, $config, $context);
}

function scc_batch_export_topics($topics, $environment, $config, &$context) {
  scc_batch_export_content('topic', $topics, $environment, $config, $context);
}

function scc_batch_export_pages($pages, $environment, $config, &$context) {
  scc_batch_export_content('page', $pages, $environment, $config, $context);
}

function scc_batch_export_taxonomies($taxonomies, $environment, $config, &$context) {
  scc_batch_export_content('taxonomy', $taxonomies, $environment, $config, $context);
}


function scc_batch_export_content($type, $items, $environment, $config, &$context) {
  module_load_include('inc', 'srrl_cache_connector', 'srrl_cache_connector_export');
  module_load_include('inc', 'srrl_cache_connector', 'srrl_cache_connector_taxonomy');
  module_load_include('inc', 'srrl_cache_connector', 'srrl_cache_connector_static');

  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max_items'] = variable_get('srrl_cache_batch_max_items', 5);
  }

  for($i = 0; $i < $context['sandbox']['max_items']; $i++) {
    $item_id    = @$items[$context['sandbox']['progress']]['id'];
    $timestamp  = @$items[$context['sandbox']['progress']]['timestamp'];

    if(empty($item_id)) {
      $context['finished'] = 1;
      return;
    }

    // Export to SailsJS only if it was selected
    if($config['to_sails']) {
      $export_function = 'scc_send_' . $type . '_to_cache';
      if( $export_function($item_id, $environment) ) {
        scc_set_content_export_timestamp($type, $item_id, $timestamp, $environment);
      }
      else {
        drupal_set_message('Error exporting ' . $type . ' ' . $item_id, 'error');
      }
    }

    // Export to static files only if the option was selected
    if($config['to_static']) {
      $static_function = 'scc_save_' . $type . '_static';
      if(function_exists($static_function)) {
        $static_function($item_id, $environment);
      }
    }

    $context['sandbox']['progress']++;
    $context['finished'] = $context['sandbox']['progress'] / count($items);
  }
}

function scc_batch_finished($success, $results, $operations) {
  if($success) {
    drupal_set_message('The content was exported. Theck the  system log for errors.');
  }
}

<?php

function scc_get_vocabulary_tree_by_name($name) {
  module_load_include('module', 'ssrl_document', 'ssrl_document');

  $vocabularies = taxonomy_vocabulary_get_names();

  // Get the vocabulary item that matches the name provided
  $vocabulary = @$vocabularies[$name];

  if(!$vocabulary) {return false;}

  // Get the taxonomy tree array
  $terms_list = taxonomy_get_tree($vocabulary->vid);
  $tree = array();
  $too_many_levels = false;
  
  // Turn the flat array into a nested one
  // Works for two levels only, not recursive
  foreach($terms_list as $index=>$term) {
    $clean_term = array(
      'tid' => $term->tid,
      'name' => $term->name,
      'weight' => $term->weight,
      'children' => false
    );


    // Load fields from a full term object
    $term_object = taxonomy_term_load($term->tid);
    foreach($term_object as $prop=>$value) {
      if(strpos($prop, 'field_') !== false && !empty($value)) {
        $prop = substr($prop, 6);
        $item = $value['und'][0];
        if(key_exists('safe_value', $item)) {
          $clean_term[$prop] = $item['safe_value'];
        }
        else if(key_exists('value', $item)) {
          $clean_term[$prop] = $item['value'];
        }
        else if(key_exists('tid', $item)) {
          $clean_term[$prop] = $item['tid'];
        }
        else if(key_exists('rgb', $item)) {
          $clean_term[$prop] = $item['rgb'];
        }
      }
    }
    $description = $term_object->description;
    $description_format = $term_object->format;
    $clean_term['description'] = check_markup($description, $description_format);

    if($term->depth == 0) {
      $tree[] = $clean_term;
    }
    else if($term->depth == 1) {
      unset($clean_term['children']);
      $parent_index = count($tree) - 1;
      $parent = &$tree[$parent_index];
      
      if(!is_array($parent['children'])) {
        $parent['children'] = array();
      }

      $parent['children'][] = $clean_term;
    }
    else {
      $too_many_levels = true;
    }
  }

  // Clean empty children arrays
  unset($term);
  foreach($tree as &$term) {
    if($term['children'] == FALSE) {
      unset($term['children']);
    }
  } 

  if($too_many_levels) {
    drupal_set_message('Too many nesting levels: only 2 taxonomy levels are allowed. Deeper levels were ignored.', 'error');
    watchdog('srrl_cache_connector', 'Warning: Too many nesting levels in vocabulary ' . $name . ': only 2 taxonomy levels are allowed. Deeper levels were ignored.');
  }

  // Allow specific post-processing by vocabulary
  $postFunction = 'scc_post_process_vocabulary_' . $name;
  if(function_exists($postFunction)) {
    $postFunction($tree);
  }
  
  return $tree;
}

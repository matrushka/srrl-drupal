<?php
module_load_include('inc', 'srrl_cache_connector', 'srrl_cache_connector_export');

function scc_prepare_container($environment) {
  $static_dir = variable_get('srrl_static_dir', FALSE);

  if(!$static_dir) {
    watchdog('srrl_cache_connector', 'Variable srrl_static_dir is not set. Pages can\'t be exported', WATCHDOG_ERROR);
    return FALSE;
  }
  else if(!is_dir($static_dir)) {
    watchdog('srrl_cache_connector','Directory set in srrl_static_dir does not exist.', WATCHDOG_ERROR);
    return FALSE;
  }

  $container = $static_dir . "/$environment/";
  if(!is_dir($container) && !mkdir($container, 0755)) {
    watchdog('srrl_cache_connector',"Can't create container directory $container.", WATCHDOG_ERROR);
    return FALSE;
  }
  else {
    return $container;
  }
}

function scc_static_assets($environment) {
  $module_path = drupal_get_path('module', 'srrl_cache_connector');
  $assets_path = realpath($module_path) . '/static-assets/';

  $files = array(
    'static-styles.css',
    'sri-logo.png',
    'country_preview_style.css'
  );


  if(!$container = scc_prepare_container($environment)) {
    return false;
  }
  else {
    foreach($files as $file) {
      $filepath = $container . $file;
      $success = copy($assets_path . $file, $container . $file);
    }
  }
}

function scc_static_countries_list($environment) {
  module_load_include('inc', 'srrl_cache_connector', 'srrl_cache_connector_taxonomy');
  $vocabulary_name = variable_get('srrl_cache_countries_vocabulary');
  $tree = scc_get_vocabulary_tree_by_name($vocabulary_name);

  $countries = array_filter($tree, function($item) {
    return $item['published'];
  });
  
  $items = array();
  foreach($countries as $country) {
    $name = $country['name'];
    $tid = $country['tid'];
    $items[] = "<a href='country-$tid.html'>$name</a>";
  }

  $content = array(
    'page_title' => 'Countries list',
    'page_content' => theme('item_list', array('items' => $items))
  );

  $html = theme('srrl_static', $content);
  $filename = 'countries-list.html';

  if(!$container = scc_prepare_container($environment)) {
    return false;
  }
  else {
    $file = $container . $filename;
    $success = file_put_contents($file, $html); 

    if($success === FALSE) {
      watchdog('srrl_cache_connector',"There was a problem writing the countries list.", WATCHDOG_ERROR);
    }
  }
}

function scc_save_page_static($page_code, $environment) {
  $page = scc_get_page($page_code);

  $contents = array(
    'page_title' => $page['title'],
    'page_content' => $page['contents']
  );

  $html = theme('srrl_static', $contents);
  $filename = 'page-' . $page_code . '.html';

  if(!$container = scc_prepare_container($environment)) {
    return false;
  }
  else {
    $file = $container . $filename;
    $success = file_put_contents($file, $html); 

    if($success === FALSE) {
      watchdog('srrl_cache_connector',"There was a problem writing the page $page_code", WATCHDOG_ERROR);
    }
  }
}

function scc_save_country_static($country_code, $environment) {
  $country = scc_get_country($country_code);
  $rendered_country = theme('country_preview', array('country' => $country));
  $contents = array(
    'page_title' => $country['doc']['country']['name'],
    'page_content' => $rendered_country
  );
  $html = theme('srrl_static', $contents);
  $filename = 'country-' . $country_code . '.html';

  if(!$container = scc_prepare_container($environment)) {
    return false;
  }
  else {
    $file = $container . $filename;
    $success = file_put_contents($file, $html); 

    if($success === FALSE) {
      watchdog('srrl_cache_connector',"There was a problem writing the page $page_code", WATCHDOG_ERROR);
    }
  }
}
?>
